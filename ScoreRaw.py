import mne, os, glob
import numpy as np


# .raw files
rawpath  = r'E:\ReboundSL\data\EGI\BMSSL_51\session1\sleep\NSfilter'

# channels used for sleep scoring
f3    = 24;
f4    = 124;
c3    = 36;
c4    = 104;
o1    = 70;
o2    = 83;
a1    = 57;
a2    = 100;
eog1L = 128;
eog1R = 1;
eog2L = 125;
eog2R = 32;
emgL  = 107;
emgR  = 113;
chInd = [f3, f4, c3, c4, o1, o2, a1, a2, eog1L, eog1R, eog2L, eog2R, emgL, emgR]
# chInd   = np.array([ 24, 124,  36, 104,  70,  83,  57, 100,    128,      1,    125,     32,   107,   113])-1;

# appending 1h chunks
data_raw = np.empty((len(chInd), 0), int)


# For all .raw files
for file in glob.glob(rawpath + '**/*.raw'):
    print(file)
    
    # Read 1h of EEG data    
    EEG_1h = mne.io.read_raw_egi(file, preload=False)
    
    # EEG_1h is now a MNE object. This is of course computer science jargon. What
    # it actually means is that you get a data structure that is more than the 
    # channels by time series and the information about channel types and 
    # locations, meta-data if you want. Indeed the structures that MNE is using 
    # provide so called methods. These are nothing but functions that are 
    # configured to take the data and the meta-data of the object as parameters. 
    # Sounds complicated, but it’s actually simplifying your life as you will see 
    # below. Whether you consider Raw objects that describe continuous data, Epochs
    # objects describing segmented single trial data, or Evoked objects describing
    # averaged data, all have in common that they share certain methods.   
    # https://mne.tools/0.17/generated/mne.io.read_raw_egi.html#mne.io.read_raw_egi
    
    # srate
    srate = EEG_1h.info['sfreq']
    
    # Select channels
    # names = [EEG_1h.ch_names[ch] for ch in chInd]
    # EEG_1h.pick_channels(names)
    data, times = EEG_1h[chInd]
    
    # concatenate 1h chunks
    data_raw = np.append(data_raw, data, axis=1)
    
# re-referencing
f3a2    = (data_raw[0]-data_raw[7]);
f4a1    = (data_raw[1]-data_raw[6]);
c3a2    = (data_raw[2]-data_raw[7]);
c4a1    = (data_raw[3]-data_raw[6]);
o1a2    = (data_raw[4]-data_raw[7]);
o2a1    = (data_raw[5]-data_raw[6]);
eog1    = (data_raw[8]-data_raw[9]);
eog2    = (data_raw[10]-data_raw[11]);
emg     = (data_raw[12]-data_raw[13]);
data_ref=[ f3a2,  f4a1,  c3a2,  c4a1,  o1a2,  o2a1,  eog1,  eog2,   emg]
chType  =['eeg', 'eeg', 'eeg', 'eeg', 'eeg', 'eeg', 'eeg', 'eeg', 'eeg']
chNames =['F3',   'F4',  'C3',  'C4',  'O1',  'O2','EOG1','EOG2', 'EMG']


    
# MNE structure
info    = mne.create_info(ch_names=chNames, ch_types=chType, sfreq=EEG_1h.info['sfreq'])
EEG_raw = mne.io.RawArray(data_ref, info)

# filter
# EEG_raw.plot_psd(area_mode='range', fmax=60, n_fft=int(4*srate), picks=np.arange(0,9), n_overlap=int(2*srate), average=False)

EEG_raw.notch_filter(np.arange(50, 201, 50), picks='all', filter_length='auto', phase='zero')
EEG_raw.filter(None, 35.,  fir_design='firwin', picks=np.arange(0,8))
EEG_raw.filter(0.8, None,  fir_design='firwin', picks=np.arange(0,8))
EEG_raw.filter(None, 100., fir_design='firwin', picks=np.arange(8,9))
EEG_raw.filter(10, None,   fir_design='firwin', picks=np.arange(8,9))

# EEG_raw.plot_psd(area_mode='range', fmax=60, n_fft=int(4*srate), picks='all', n_overlap=int(2*srate), average=False)
# EEG_raw.plot_psd(area_mode='range', fmax=5, n_fft=int(4*srate), picks='all', n_overlap=int(2*srate), average=False)

# down-sample
EEG_raw.resample(128, npad="auto")
    
# # Filter
# mne.filter.create_filter(EEG_raw, EEG_raw.info['sfreq'], 0.5, None, method='fir', phase='zero')

# EEG_raw.plot()

# Sleep scoring, open the GUI :
from visbrain.gui import Sleep
Sleep(data=EEG_raw._data, sf=EEG_raw.info['sfreq'], channels=EEG_raw.info['ch_names']).show()
















